// 1598. Crawler Log Folder

/*
    METHOD-1
    |-> maintain a depth count,
    |-> iterate the array, check the first character is '.', 
    |-> if yes, check the second character is also '.' , this means one directory up, so reduce the depth if it is greater than 0
    |-> else continue for inner if 
    |-> else increase the depth count;

*/

/*
JAVASCRIPT METHOD-1

var minOperations = function(logs) {
    let depth = 0;
    for(let log of logs){
        if(log[0]=='.'){
            if(log[1]=='.' && depth>0){
                depth--;
            }
        }else depth++;
    }

    return depth;
};

*/