// 726. Number of Atoms

/*
    METHOD-1
    |-> use stack to store the different chunks of formula
    |-> for every character check for opening or closing parenthesis
    |-> if opening parenthesis push empty object in stack 
    |-> if closing parenthesis, count the next continous digits and update the topmost stack objects values. Pop the top value and merge it with the stack's current top object
    |-> if it is not a parenthesis, get the elementName then get the elemtentCount and update in the stack's top object 
    |-> at end sort the entries and combime the element name and their count



*/



/*

    JAVASCRIPT METHOD-1

var countOfAtoms = function(formula) {
    let index=0, length = formula.length;
    let allElementsAndTheirCounts = [{}], currChunk = {};

    while(index<length){
        if(formula[index]=='('){
            allElementsAndTheirCounts.push({});
            index++;
        }else if(formula[index]==')'){
            index++;
            let outerNumberCount = getElementCount();
            multiplyByNInCurrentChunk(outerNumberCount);
            mergeAllElementAndCurrChunk();
        }else {
        let elementName = getElementName();
        let elementCount = getElementCount();
        allElementsAndTheirCounts[allElementsAndTheirCounts.length-1][elementName] = allElementsAndTheirCounts[allElementsAndTheirCounts.length-1][elementName] ?? 0;
        allElementsAndTheirCounts[allElementsAndTheirCounts.length-1][elementName]+=elementCount;
        }
    }

    let sortedEntries = Object.entries(allElementsAndTheirCounts[0]).sort((element1, element2)=>{
        return element1[0].localeCompare(element2[0]);
    });


    let solutionString = sortedEntries.reduce((accumulator, [elementName, elementCount])=>{
        if(elementCount == 1){
            elementCount="";
        }
        accumulator+=elementName + elementCount;
        return accumulator;
    },"");

    return solutionString;

    function getElementName(){
        let elementName = "";
        let firstLetter = true;
        while(index<length){
            if(firstLetter){
                if(isUpperCaseLetter(formula[index])==false)break;
                firstLetter=false;
            }else if(isLowerCaseLetter(formula[index])==false)break;
            elementName+= formula[index];
            index++;
        }
        return elementName;
    }

    function isUpperCaseLetter(character){
        return character>='A' && character<='Z';
    }
    function isLowerCaseLetter(character){
        return character>='a' && character<='z';
    }

    function getElementCount(){
        let elementCount="";
        while(index<length && formula[index]>='0' && formula[index]<='9'){
            elementCount+=formula[index];
            index++;
        }
        return elementCount.length > 0 ? Number(elementCount) : 1;
    }

    function mergeAllElementAndCurrChunk(){
        let currChunk = allElementsAndTheirCounts.pop();
        for(let [elementName, elementCount] of Object.entries(currChunk)){
            allElementsAndTheirCounts[allElementsAndTheirCounts.length-1][elementName] = allElementsAndTheirCounts[allElementsAndTheirCounts.length-1][elementName] ?? 0;
            allElementsAndTheirCounts[allElementsAndTheirCounts.length-1][elementName] += elementCount;
        }
    }


    function multiplyByNInCurrentChunk(outerNumberCount){
        let updatedCurrChunk = allElementsAndTheirCounts[allElementsAndTheirCounts.length-1];
        for(let [elementName, elementCount] of Object.entries(updatedCurrChunk)){
            updatedCurrChunk[elementName] = elementCount*outerNumberCount;
        }
    }

};

*/