// 6. Zigzag Conversion


/*
    METHOD-1
    |-> use array of size numRows
    |-> maintain currIndex and change variable , where change shows the currIndex movement i.e 0->numRows or 0<-numRows 
    |-> whenever currIndex+change reaches any extreme end change the direction and currIndex accordingly
    |-> after iteration, concat all strings from the array and return it.
    |-> only one edge case : if numRows is 1 (one) return the string as it is

    METHOD-2
    |-> using arithmatic, for every index we need to find the distance it needs to cover till next valid index
    |-> we will start index from 0 to numRows-1, where currIndex will initialize with index 
    |-> imagine a row of string of numRows length, at any index we need to alternatively find the bottomTravle then topTravel, 
    |-> bottomTravel is the distance from currIndex to bottom of rows and then back to current Row, 
        eg: 1      7
            2    6 8
            3  5   9
            4      10
    |-> here for value 2 we need to cover bottomTravel i.e 3->4->5->6, 
    |-> for value 5, we need to cover topTravel i.e 6->7->8->9
    |-> the calculation for that would be bottomTravel = 2(numRows-currIndex)-2
    |-> for topTravel = currIndex*2
    |-> NOTE: if the starting index is 0 or numRows-1 then we simply increase currIndex by 2*numRows-1
    |-> see the code to get better understandings

*/



/*
    JAVASCRIPT-METHOD-1

var convert = function(s, numRows) {
    if(numRows==1)return s;
    let zigZagArray= new Array(numRows).fill("");
    let currIndex=0, change=1;
    for(let currCharacter of s){
        zigZagArray[currIndex]+=currCharacter;
        currIndex+=change;
        if(currIndex==numRows){
            currIndex=numRows-2;
            change=-1;
        }
        if(currIndex<0){
            currIndex=1;
            change=1;
        }
    }
    let solutionString = "";
    for(let zigZagString of zigZagArray){
        solutionString+=zigZagString;
    }

    return solutionString;
};



JAVASCRIPT METHOD-2

var convert = function(s, numRows) {
    if(numRows == 1)return s;
    let solutionString = "";
    let length = s.length;
    
    for(let index=0;index<numRows;index++){
        let currIndex=index;
        let bottomTravel = 2*(numRows-currIndex)-2;
        let topTravel = index*2;
        let moveTop = false;
        while(currIndex<length){
            solutionString+=s[currIndex];
            if(index!=0 && index!=numRows-1){
                if(moveTop){
                    currIndex+=topTravel;
                    moveTop = false;
                }else {
                    currIndex+=bottomTravel;
                    moveTop = true;
                }
            }else {
               currIndex+=2*numRows-2;
            }
        }
    }

    return solutionString;
};
*/