// 24. Swap Nodes in Pairs

/*
    METHOD-1
    |-> create a prevnode and solution node initialized with any value (eg: -1)
    |-> link the prevnode to head
    |-> iterate through the nodes with condition of : head and head.next are not null
    |-> prevnode => head => head.next => nextIterationNode
    |-> for every iteration 
        |-> store nextIterationNode
        |-> link prevnode to head.next      prevnode => head.next => nextIterationNode
                                                    head /
        |-> link head.next to head         prevnode => head.next => head    nextIterationNode
                                                            \-------/
        |-> make(not link) prevnode to head 
        |-> link head to nextIterationNode  prevnode => head.next => head => nextIterationNode
        |-> move the head to nextIterationNode
*/

/*
    JAVASCRIPT METHOD-1
    
    var swapPairs = function(head) {
    let prevNode = new ListNode(-1);
    prevNode.next=head;
    let solutionNode=prevNode;
    while(head!=null && head.next!=null){
        let nextIterationHead = head.next.next;
        prevNode.next = head.next;
        head.next.next=head;
        head.next=nextIterationHead;
        prevNode=head;
        head=head.next;
    }

    return solutionNode.next;
};

*/