// 1701. Average Waiting Time

/*
    METHOD-1
    |-> maintain a startTime variable initialize with first value of array ie arr[0][0], and totalTime 
    |-> traverse the 2-d array , the exactStartTime will be max of prev starttime and current customer's startTime 
    |-> endtime will be exactStartTime + prepTime, 
    |-> add the diff of endtime and currStartTime to the totalValue
    |-> return the totalTime/arr.length


*/

/*
    JAVASCRIPT METHOD-1

    /**
 * @param {number[][]} customers
 * @return {number}
 */
var averageWaitingTime = function(customers) {
    let n = customers.length;
    let totalTime = 0;
    let startTime = customers[0][0];
    
    for(let [currStartTime, prepTime] of customers){
        let exactStartTime = Math.max(startTime, currStartTime);
        let endTime = exactStartTime + prepTime;
        totalTime += endTime - currStartTime;
        startTime = endTime;
    }
    return totalTime/n;
};


*/