// 2976. Minimum Cost to Convert String I

/*
    METHOD-1
    |-> create a object of character and their changable character with cost, { a : {b:5, c:4}, b : {d:4, e:2}, ...}
    |-> NOTE: while creating the object, a character can have multiple changable character, but update the cost to lowest one possible
    |-> traverse the source and target string simultaneously, get the cost required to change the source character to target character 
    |-> the cost can be obtained by using graph based BFS on the created object, if not possible, return Infinity
    |-> use a Map (for DP), to store the result of a source character and it's array of changable character cost 
    |-> refer the code for better understanding 
    |-> for every character add the cost, if not possible immediately return -1

*/

/*
    JAVASCRIPT METHOD-1
   
var minimumCost = function (source, target, original, changed, cost) {
    let map = {};
    let length = original.length;
    for (let index = 0; index < length; index++) {
        let originalChar = original[index];
        let changedChar = changed[index];
        let changeCost = cost[index];

        //setting default
        map[originalChar] = map[originalChar] ?? {};
        let alreadyChangeCost = map[originalChar][changedChar];
        if (!alreadyChangeCost) {
            alreadyChangeCost = Infinity;
        }
        if (changeCost > alreadyChangeCost) {
            changeCost = alreadyChangeCost;
        }
        map[originalChar][changedChar] = changeCost;
    }

    // transform string
    let dp = new Map();
    let totalCost = 0;
    for (let index = 0; index < source.length; index++) {
        let originalChar = source[index];
        let changeChar = target[index];
        let changeCost = changeCostHelper(originalChar, changeChar, map, dp);
        if (changeCost != Infinity) {
            totalCost += changeCost;
        } else return -1;
    }
    return totalCost;
};

function changeCostHelper(originalChar, changedChar, map, dp) {
    let distance = new Array(26).fill(Infinity);
    let aCharCode = "a".charCodeAt(0);
    if(dp.has(originalChar)){
        return dp.get(originalChar)[changedChar.charCodeAt(0)-aCharCode];
    }
    distance[originalChar.charCodeAt(0) - aCharCode] = 0;
    let queue = [originalChar];

    while (queue.length > 0) {
        let topChar = queue.shift();
        if ((topChar in map) == false) {
            continue;
        }
        let topCharCost = distance[topChar.charCodeAt(0) - aCharCode];
        let sideCharArray = Object.entries(map[topChar]);

        for (let [sideChar, currCost] of sideCharArray) {
            let currTotalCost = topCharCost + currCost;
            if (distance[sideChar.charCodeAt(0) - aCharCode] > currTotalCost) {
                distance[sideChar.charCodeAt(0) - aCharCode] = currTotalCost;
                queue.push(sideChar);
            }
        }
    }
    dp.set(originalChar, distance);

    return distance[changedChar.charCodeAt(0) - aCharCode];
}

*/