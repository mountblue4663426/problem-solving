/*
CONTAINS DUPLICATE

METHOD-1
-> create a set, add values in it, if we see a value already present return true
-> at end return false;

METHOD-2
-> sort the array
->traverse from index 1 and equate with previsous index value , if same return true;
-> at end return false;

METHOD-3







*/


//JAVASCRIPT -METHOD-1
/*

var containsDuplicate = function(nums) {
    let visited = {};

    for(let value of nums){
        if(visited[value])return true;
        visited[value]=true;
    }

    return false;

};


*/



/*
JAVASCRIPT METHOD-2

var containsDuplicate = function(nums) {
    nums.sort((value1, value2)=>{
        return value1 - value2;
    });

    for(let index=1;index<nums.length;index++){
        if(nums[index] == nums[index-1])return true;
    }

    return false;

};


*/



/*

JAVASCRIPT METHOD-3






/*